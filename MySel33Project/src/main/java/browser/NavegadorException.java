package browser;

public class NavegadorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String methodName;
	private String [] params;
	private String msg;
	
	public NavegadorException(String methodName, String[] params, String msg) {
		super();
		this.methodName = methodName;
		this.params = params;
		this.msg = msg;
	}

	@Override
	public String getMessage() {
		StringBuilder msg = new StringBuilder();
		msg.append("-------------------------------\nNavegador\n");
		msg.append(this.methodName).append("\n");
		for (int i = 0; i < this.params.length; i++) {
			msg.append(this.params[i]).append(" ");
		}
		msg.append("\n");
		msg.append(this.msg).append("\n");
		msg.append("-------------------------------");
		
		return msg.toString();
	}
	 
	
}
