package browser;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Navegador {

	private WebDriver driver;
	private WebElement el;
	private List<WebElement> els;
	private WebDriverWait wait;
	private String _name;
	private String _text;

	public void open() {
		driver = new FirefoxDriver(Preferencias.profileFireFox());
		wait = new WebDriverWait(driver, Config.WAIT_SECONDS);
	}

	public void close() {
		if (driver != null)
			driver.quit();
	}

	public void goTo(String url) {
		driver.get(url);
	}

	public String getTextFromH3(int indexH3) {
		els = driver.findElements(By.tagName("h3"));
		try {
			return els.get(indexH3).getText();
		} catch (Exception e) {
			return e.getMessage();
		}

	}

	public void setInputText(String name, String value) {
		els = driver.findElements(By.tagName("input"));
		for (WebElement we : els) {
			if (we.getAttribute("name").equals(name)) {
				we.sendKeys(value);
				break;
			}
		}
	}

	public void clickButton(String text) {
		clickByTextTag("button", text);
	}

	public void clickButtonTitle(String title, int indexBtn) {
		els = driver.findElements(By.tagName("button"));
		for (WebElement we : els) {
			if (we.getAttribute("title").equals(title)) {
				we.click();
				break;
			}
		}
	}

	public void clickLink(String text) {
		clickByTextTag("a", text);
	}

	public void clickByTextTag(String tag, String text) {
		els = driver.findElements(By.tagName(tag));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				we.click();
				break;
			}
		}
	}

	public void waitModalOpen(String klass) {
		WebElement modal = driver.findElements(By.className(klass)).get(0);
		el = wait.until(ExpectedConditions.visibilityOf(modal));
	}

	public void waitModalClose(String klass) {
		WebElement modal = driver.findElements(By.className(klass)).get(0);
		wait.until(ExpectedConditions.invisibilityOf(modal));
	}

	public void radioSelect(String name, String title) {
		_name = name;
		_text = title;
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver d) {
				els = driver.findElements(By.tagName("input"));
				for (WebElement we : els) {
					if (we.getAttribute("type").equals("radio") && we.getAttribute("name").equals(_name)
							&& we.getAttribute("title").equals(_text)) {
						we.click();
					}
				}
				return true;
			}
		});

	}

	public boolean isLink(String text, int secondsWaiting) {
		driver.manage().timeouts().implicitlyWait(secondsWaiting, TimeUnit.SECONDS);
		el = null;
		el = driver.findElement(By.linkText(text));
		if (el != null) {
			return true;
		} else {
			return false;
		}
	}

	public void scroll(int pixels) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0," + pixels + ")", "");

	}

	public void focusLink(String text) {
		els = driver.findElements(By.tagName("a"));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				Actions focus = new Actions(driver);
				focus.moveByOffset(-20, -50);

				break;
			}
		}

	}

	public void scrollAndClickLink(String text) {
		els = driver.findElements(By.tagName("a"));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				el = we;
				break;
			}
		}
		scrollTo(el);
		el.click();
	}

	public void scrollAndSelect(String text) {
		els = driver.findElements(By.tagName("option"));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				el = we;
				break;
			}
		}
		scrollTo(el);
		el.click();
	}

	public void scrollAndSelect(String nameField, String text) {
		WebElement field = driver.findElements(By.name(nameField)).get(0);

		els = field.findElements(By.tagName("option"));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				el = we;
				break;
			}
		}
		scrollTo(el);
		el.click();
	}

	public void scrollAndSelectBootstrapMultiple(String idContainer, int indexBootstrapSelect, String[] text) {

		WebElement container = driver.findElement(By.id(idContainer));

		WebElement bsSelect = container.findElements(By.className("bootstrap-select")).get(indexBootstrapSelect);
		bsSelect.click();

		els = bsSelect.findElements(By.tagName("li"));

		for (WebElement li : els) {

			if (li.getAttribute("class").equals("selected")) {
				scrollTo(li);
				li.click();
			}
		}

		for (WebElement li : els) {
			el = li.findElements(By.className("text")).get(0);
			for (String string : text) {
				if (el.getText().equals(string)) {
					scrollTo(li);
					li.click();
					break;

				}
			}

		}
		bsSelect.click();
	}

	public void scrollAndSelectBootstrapSingle(String idContainer, int indexBootstrapSelect, String text) {

		WebElement container = driver.findElement(By.id(idContainer));

		WebElement bsSelect = container.findElements(By.className("bootstrap-select")).get(indexBootstrapSelect);
		bsSelect.click();
		
		els = bsSelect.findElements(By.tagName("li"));

		for (WebElement li : els) {
			el = li.findElements(By.className("text")).get(0);
			if (el.getText().equals(text)) {
				scrollTo(li);
				li.click();
				break;
			}
		}
		
	}

	public void scrollAndGoToLink(String text) {
		els = driver.findElements(By.tagName("a"));
		for (WebElement we : els) {
			if (we.getText().equals(text)) {
				el = we;
				break;
			}
		}
		scrollTo(el);
		String urlTarget = el.getAttribute("href");
		goTo(urlTarget);
	}

	private void scrollTo(WebElement webElement) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // not sure why the sleep was needed, but it was needed or it wouldnt
			// work :(
	}

	public String getTextFromClassActive(int indexClassActive) {
		return getTextFromClass("active", indexClassActive);
	}

	public String getTextFromClass(String klass, int indexKlass) {
		els = driver.findElements(By.className(klass));
		try {
			return els.get(indexKlass).getText();
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	public String getTextFromClassInClass(String classParent, int indexParent, String classChild, int indexChild) {
		els = driver.findElements(By.className(classParent));
		el = els.get(indexParent).findElements(By.className(classChild)).get(indexChild);
		return el.getText();
	}

	public void inputText(int indexInputText, String value) {
		els = driver.findElements(By.tagName("input"));
		el = els.get(indexInputText);
		el.clear();
		el.sendKeys(value);
	}

	public void inputTextName(String name, String value) {
		// els = driver.findElements(By.name(name));
		el = driver.findElement(By.name(name));
		el.clear();
		el.sendKeys(value);
	}

	public void clickAttr(String tag, String attr, String text) {
		els = driver.findElements(By.tagName(tag));
		for (WebElement webElement : els) {
			if (webElement.getAttribute(attr).equals(text)) {
				webElement.click();
				break;
			}
		}
	}

	public void clickById(String id) {
		driver.findElement(By.id(id)).click();
	}

	public void waitInvisibilityId(String id) {
		el = driver.findElement(By.id(id));
		wait.until(ExpectedConditions.invisibilityOf(el));
	}

	public String getMsgResultFromModal(String modal) {
		el = driver.findElements(By.className(modal)).get(0);
		WebElement containerResult = el.findElements(By.className("containerResult")).get(0);
		WebElement msg = containerResult.findElements(By.tagName("span")).get(1);
		return msg.getText();
	}

	public void closeModalResult(String modal) {
		el = driver.findElements(By.className(modal)).get(0);
		WebElement containerResult = el.findElements(By.className("containerResult")).get(0);
		containerResult.findElements(By.className("close")).get(0).click();
	}

	public void clickButtonClassInIdContainer(String id, String klass, int indexClass) {
		el = driver.findElement(By.id(id));
		el.findElements(By.className(klass)).get(indexClass).click();
	}

	public String getTextFromTableInIdContainer(String idContainer, int indexTr, int indexTd) {
		el = driver.findElement(By.id(idContainer));
		WebElement table = el.findElements(By.tagName("table")).get(0);
		WebElement tableBody = table.findElement(By.tagName("tbody"));
		WebElement tr = tableBody.findElements(By.tagName("tr")).get(indexTr);
		WebElement td = tr.findElements(By.tagName("td")).get(indexTd);
		return td.getText();
	}

	public void clickTagInClassContainerInIdContainer(String idContainer, String klassContainer,
			int indexKlassContainer, String tag, int indexTag) {
		el = driver.findElement(By.id(idContainer));
		WebElement classContainer = el.findElements(By.className(klassContainer)).get(indexKlassContainer);
		classContainer.findElements(By.tagName(tag)).get(indexTag).click();
	}

}
